# 汇总25个国内外ChatGPT中文版镜像网站（2024/6/26）

#### 介绍
汇总25个国内外ChatGPT中文版镜像网站给大家使用，现在国内很多镜像网站因为OpenAI禁止国内团队访问API，所以无法访问，现在给大家整理的是一些国内可以直接使用，而且不受OpenAI限制的镜像网站，部分网站支持无限次数使用，我按照个人的喜好进行排序，大家可以挑选自己喜欢的。

#### ChatGPT国内中文版镜像网站
1. https://snakegpt.work
2. https://gptcat.top/
3. https://ai-panda.xyz/login?invite_code=34137c47
4. http://gptdog.online/
5. https://ai.minihuo.com/#/chat/1002
6. https://freechatgpt.lol/
7. https://chatai.lra.cn/#/home/chat
8. https://chat.haoce.com/
9. https://vipag4.aibeke.com/
10. https://www.chatnext.top
11. https://snakegpt.work?inVitecode=PUWFCNERUN
12. https://www.chatnext.top
13. https://ai117.com/
14. https://freechatgpt.lol/
15. https://poe.com
16. http://chat8.zjqywl.cn/chatbotai/1002?channel=360toufangzt9
17. http://gp.yangxiwl.cn/chatmax/chat/1691915246543?channel=baidumaxguizhou&bd_vid=11561269466905197062
18. https://chatai.lra.cn/#/home/chat
19. https://yiyan.baidu.com/welcome
20. https://xinghuo.xfyun.cn/
21. https://tongyi.aliyun.com/
22. https://chat-ppt.com/
23. https://chatexcel.com/
24. https://kimi.moonshot.cn/
25. chatgpt.com
26. https://chat.uniation.net/